# syntax=docker/dockerfile:1
FROM openjdk:11-jre

COPY target/*.jar user-service.jar

ENTRYPOINT ["java", "-jar", "/user-service.jar"]