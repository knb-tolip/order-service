package com.bnksolutions.os.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bnksolutions.os.service.OrderService;
import com.bnksolutions.os.shared.response.OrderResponse;

@RestController
@RequestMapping("orders")
public class UserOrderController {
	@Autowired
	private OrderService service;
	
	@GetMapping("/{userId}")
	public OrderResponse getOrder(@PathVariable("userId") String userId) {
		return service.getOrdersByUser(userId);
	}
}
