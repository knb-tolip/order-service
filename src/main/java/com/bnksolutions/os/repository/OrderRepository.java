package com.bnksolutions.os.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bnksolutions.os.shared.OrderStorage;
import com.bnksolutions.os.shared.dto.OrderDTO;

@Component
public class OrderRepository {
	@Autowired
	private OrderStorage storage;

	public List<OrderDTO> getUserOrders(String userId) {
		return storage.getUserOrders(userId);
	}
}
