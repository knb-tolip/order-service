package com.bnksolutions.os.service;

import com.bnksolutions.os.shared.response.OrderResponse;


public interface OrderService {
	OrderResponse getOrdersByUser(String userId);
}
