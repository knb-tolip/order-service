package com.bnksolutions.os.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bnksolutions.os.repository.OrderRepository;
import com.bnksolutions.os.shared.response.OrderResponse;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository repo;
	@Override
	public OrderResponse getOrdersByUser(String userId) {
		OrderResponse rs = new OrderResponse();
		rs.setData(repo.getUserOrders(userId));
		return rs;
	}

}
