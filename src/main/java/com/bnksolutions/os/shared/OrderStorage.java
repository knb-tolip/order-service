package com.bnksolutions.os.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.bnksolutions.os.shared.dto.OrderDTO;

@Component
public class OrderStorage {
	private Map<String, List<OrderDTO>> database = new HashMap<>();
	
	@PostConstruct
	public void init() {
		List<OrderDTO> aList = null;
		OrderDTO dto = null;
		for(int i = 10; i < 30 ; i++) {
			aList = new ArrayList<>();
			for(int j = 100; j < 103; j ++) {
				dto = new OrderDTO();
				dto.setOrderCode("user_" +i+"_Code_" + j);
				dto.setOrderId("OID_user_" + i + "__" + j);
				dto.setOrderName("OD_NAME_" + i +"_" + j);
				dto.setOrderStatus("PROCESSING");
				aList.add(dto);
			}
			database.put("" + i, aList);
		}
	}
	
	public List<OrderDTO> getUserOrders(String userId){
		return database.get(userId);
	}
}
