package com.bnksolutions.os.shared.response;

import java.util.List;

import com.bnksolutions.os.shared.dto.OrderDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderResponse {
	private List<OrderDTO> data;
}
